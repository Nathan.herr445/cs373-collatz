#!/usr/bin/env python3.11

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# ----------
# Collatz.py
# ----------

# ------------
# collatz_eval
# ------------
cache: dict[int, int] = {}
def collatz_eval(t: tuple[int, int]) -> tuple[int, int, int]:
    """
    :param t tuple of the range
    :return max cycle length of the range, inclusive
    """
    i, j = t
    assert i > 0
    assert j > 0
    v: int = find_max((min(i, j), max(i, j)))  # fix!
    assert v > 0
    return i, j, v


def find_max(t: tuple[int, int]) -> int:
    i, j = t
    assert i > 0
    assert j > 0
    max_cycles = 0
    for x in range(i, j + 1):
        cycle = collatz(x)
        if cycle > max_cycles:
            max_cycles = cycle
    assert max_cycles > 0
    return max_cycles


def collatz(n: int) -> int:
    assert n > 0
    num: int = n  # stores original value for caching
    c: int = 1
    while n > 1:
        if n <= num:
            if cache.get(n) is not None:
                c += cache[n] - 1
                break
        if (n % 2) == 0:
            n = n // 2
            c += 1
        else:
            n = n + (n >> 1) + 1
            c += 2  # skipped a second loop, add 2 to reflect this
    if cache.get(num) is None:  # checks to see if number needs to be cached
        cache[num] = c
    assert c > 0
    return c
