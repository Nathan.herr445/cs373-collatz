#!/usr/bin/env python3.11

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# ---------------
# test_Collatz.py
# ---------------

# -------
# imports
# -------

import unittest # main, TestCase

from Collatz import collatz_eval
from Collatz import collatz
from Collatz import find_max

# ------------
# Test_Collatz
# ------------

class Test_Collatz (unittest.TestCase) :
    def test_collatz_0 (self) -> None :
        self.assertEqual(collatz_eval((1, 10)), (1, 10, 20))

    def test_collatz_1 (self) -> None :
        self.assertEqual(collatz_eval((100, 200)), (100, 200, 125))

    def test_collatz_2 (self) -> None :
        self.assertEqual(collatz_eval((201, 210)), (201, 210, 89))

    def test_collatz_3 (self) -> None:
        self.assertEqual(collatz_eval((900, 1000)), (900, 1000, 174))

    #Test reverse input
    def test_collatz_4 (self) -> None:
        self.assertEqual(collatz_eval((10 , 1)), (10, 1, 20))

    #test that range is inclusive
    def test_collatz_5 (self) -> None:
        self.assertEqual(collatz_eval((1 , 3)), (1, 3, 8))

    #test max range
    def test_collatz_6 (self) -> None:
        self.assertEqual(collatz_eval((1 , 999999)), (1, 999999, 525))

    def test_collatz_7 (self) -> None:
        self.assertEqual(collatz_eval((999999 , 1)), (999999, 1, 525))
    
    def test_collatz_8 (self) -> None:
        with self.assertRaises(AssertionError):
            collatz_eval((0,10))

    def test_collatz_9 (self) -> None:
        with self.assertRaises(AssertionError):
            collatz_eval((10,0))

    def test_collatz_10 (self) -> None:
        with self.assertRaises(AssertionError):
            collatz_eval((0,0))

    def test_find_max_0 (self) -> None :
        self.assertEqual(find_max((1, 10)), (20))

    def test_find_max_1 (self) -> None :
        self.assertEqual(collatz_eval((100, 200)), (100, 200, 125))

    def test_find_max_2 (self) -> None :
        self.assertEqual(find_max((201, 210)), (89))

    def test_find_max_3 (self) -> None:
        self.assertEqual(find_max((900, 1000)), (174))

    #verifies both i & j > 0
    def test_find_max_4 (self) -> None:
        with self.assertRaises(AssertionError):
            find_max((0, 10))

    def test_find_max_5 (self) -> None:
        with self.assertRaises(AssertionError):
            find_max((0, 0))
    
    #test indivudual numbers
    def test_collatz_funtion_1 (self) -> None:
        self.assertEqual(collatz(10), (7))

    def test_collatz_funtion_2 (self) -> None:
        self.assertEqual(collatz(1), (1))

    def test_collatz_funtion_3 (self) -> None:
        self.assertEqual(collatz(999999), (259))
    #verifies falure if n not > 0
    def test_collatz_funtion_4 (self) -> None:
        with self.assertRaises(AssertionError):
            collatz(0)

    

# ----
# main
# ----

if __name__ == "__main__" : #pragma: no cover
    unittest.main()
